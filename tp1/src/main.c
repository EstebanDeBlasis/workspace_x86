/*==================[inclusions]=============================================*/

#include "main.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input;
    int8_t EstadoBarrera=0;

    hw_Init();

    // Superloop hasta que se presione la tecla Esc
    while (input != EXIT) {
        input = hw_LeerEntrada();

        if (input == SENSOR_1) {

            if (!EstadoBarrera){
                hw_AbrirBarrera();
                EstadoBarrera=1;   
            }else{
                hw_VeiculoIngresando();
            }
            
        }

        if (input == SENSOR_2) {

            if(EstadoBarrera){
            
                do
                {
                    hw_Pausems(5000);
                    input = hw_LeerEntrada();
                } while (input!=SENSOR_2);
            
                hw_CerrarBarrera();
                EstadoBarrera=0;

            }else
            {
                do
                {
                    hw_SonarAlarma();
                    input=hw_LeerEntrada();
                } while (input!=PARAR_ALARMA);  
                hw_PararAlarma ();              
            }
            

                      
        }

        
        
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
