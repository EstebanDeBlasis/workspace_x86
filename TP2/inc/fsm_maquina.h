#ifndef _MAQUINA_H_
#define _MAQUINA_H_

/*==================[inclusions]=============================================*/

/*==================[macros]=================================================*/

/*==================[typedef]================================================*/

typedef enum {
    ESPERANDO_FICHA,
    ESPERANDO_ELECCION,
    SIRVIENDO_TE,
    SIRVIENDO_CAFE,
    ALARMA
} FSM_MAQUINA_STATES_T;

/*==================[external data declaration]==============================*/

/*==================[external functions declaration]=========================*/

// Inicializacion y evaluacion de la FSM
void fsm_maquina_init(void);
void fsm_maquina_runCycle(void);

// Eventos
void fsm_maquina_raise_evSensorFicha(void);
void fsm_maquina_raise_evSensorCafe(void);
void fsm_maquina_raise_evSensorTe(void);
void fsm_maquina_raise_evTick1seg(void);
void fsm_maquina_raise_ev1HZ(void);
void fsm_maquina_raise_ev5HZ(void);

// Debugging
void fsm_maquina_printCurrentState(void);

/*==================[end of file]============================================*/
#endif /* #ifndef _MAQUINA_H_ */
