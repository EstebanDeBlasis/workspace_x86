/*==================[inclusions]=============================================*/

#include "main.h"
#include "fsm_maquina.h"
#include "hw.h"

/*==================[macros and definitions]=================================*/

/*==================[internal data declaration]==============================*/

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

/*==================[external functions definition]==========================*/

int main(void)
{
    uint8_t input = 0;
    uint16_t cont_ms = 0;
    uint16_t cont2_ms = 0;
    uint16_t cont3_ms = 0;
        

    hw_Init();

    fsm_maquina_init();

    while (input != EXIT) {
        input = hw_LeerEntrada();

        // En un microcontrolador estos eventos se generarian aprovechando las
        // interrupciones asociadas a los GPIO
        if (input == SENSOR_FICHA) {
            fsm_maquina_raise_evSensorFicha();
        }

        if (input == SENSOR_CAFE) {
            fsm_maquina_raise_evSensorCafe();
        }
		
		if (input == SENSOR_TE) {
            fsm_maquina_raise_evSensorTe();
        }
		
        // En un microcontrolador esto se implementaria en un handler de
        // interrupcion asociado a un timer
        cont_ms++;
        if (cont_ms == 500) {
            cont_ms=0;
            fsm_maquina_raise_ev1HZ();
        }
        
        // En un microcontrolador esto se implementaria en un handler de
        // interrupcion asociado a un timer
        cont2_ms++;
        if (cont2_ms ==100){
            cont2_ms=0;
            fsm_maquina_raise_ev5HZ();
        }
        
        // En un microcontrolador esto se implementaria en un handler de
        // interrupcion asociado a un timer
        cont3_ms++;
        if (cont3_ms ==1000){
            cont3_ms=0;
            fsm_maquina_raise_evTick1seg();
        }

        fsm_maquina_runCycle();

        // Esta funcion hace que el sistema no responda a ningun evento por
        // 1 ms. Funciones bloqueantes de este estilo no suelen ser aceptables
        // en sistemas baremetal.
        hw_Pausems(1);
        //fsm_maquina_printCurrentState();
    }

    hw_DeInit();
    return 0;
}

/*==================[end of file]============================================*/
