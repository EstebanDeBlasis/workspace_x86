/*==================[inclusions]=============================================*/

#include "fsm_maquina.h"
#include "hw.h"
#include <stdio.h>
#include <stdbool.h>

/*==================[macros and definitions]=================================*/

#define ESPERA_ELECCION_SEG  30
#define ESPERA_ALARMA_SEG  2
#define ESPERA_TE_SEG  30
#define ESPERA_CAFE_SEG  45

/*==================[internal data declaration]==============================*/

static bool estado_indicador = false;

/*==================[internal functions declaration]=========================*/

/*==================[internal data definition]===============================*/

static FSM_MAQUINA_STATES_T state;

static bool evSensorFicha_raised;
static bool evSensorCafe_raised;
static bool evSensorTe_raised;
static bool evTick1seg_raised;
static bool ev1HZ_raised;
static bool ev5HZ_raised;
static uint8_t count_seg;

/*==================[external data definition]===============================*/

/*==================[internal functions definition]==========================*/

static void clearEvents(void)
{
    evSensorFicha_raised = 0;
    evSensorCafe_raised = 0;
    evSensorTe_raised = 0;
    evTick1seg_raised = 0;
    ev1HZ_raised = 0;
    ev5HZ_raised = 0;
}

static void toggleIndicador(void)
{
    printf ("\e[A");

    if (estado_indicador)
    {
        hw_IndicadorApagado();
        estado_indicador=false;
    }
    else 
    {
        hw_IndicadorEncendido();
        estado_indicador=true;
    }
    
}

/*==================[external functions definition]==========================*/

void fsm_maquina_init(void)
{
    system("clear");
    state = ESPERANDO_FICHA;
    clearEvents();
}

void fsm_maquina_runCycle(void)
{
    // El diagrama se encuentra en fsm_maquina/media/fsm_maquina_diagrama.png
    switch (state) {
        case ESPERANDO_FICHA:
        	
            if (evSensorFicha_raised) {
                hw_IndicadorApagado();
                estado_indicador=false;
                state = ESPERANDO_ELECCION;
            }
 
            if (ev1HZ_raised){
                toggleIndicador();
            	ev1HZ_raised=0;
            	state = ESPERANDO_FICHA;
            	}
            break;

        case ESPERANDO_ELECCION:
            if (evSensorTe_raised) {
                count_seg = 0;
                hw_ServirTe();
                state = SIRVIENDO_TE;
            }
            else if (evSensorCafe_raised){
                count_seg = 0;
                hw_ServirCafe();
                state = SIRVIENDO_CAFE;
            }
            
            if (evTick1seg_raised && (count_seg < ESPERA_ELECCION_SEG)) {
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_ELECCION_SEG)) {
                hw_DevolverFicha();
                count_seg=0;    
                state = ESPERANDO_FICHA;
            }
            
            break;

        case SIRVIENDO_TE:
            if (evTick1seg_raised && (count_seg < ESPERA_TE_SEG)) {
            	count_seg++;
                state = SIRVIENDO_TE;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_TE_SEG)) {
                hw_TeServido();
            	count_seg=0;
                ev5HZ_raised=0;
                hw_IndicadorApagado();
                estado_indicador=false;
                state = ALARMA;
            }
            
            if (ev5HZ_raised){
                toggleIndicador();
            	ev5HZ_raised=0;
            	state = SIRVIENDO_TE;
            }
            	
            break;
            
        case SIRVIENDO_CAFE:

            if (evTick1seg_raised && (count_seg < ESPERA_CAFE_SEG)) {
            	count_seg++;
                state = SIRVIENDO_CAFE;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_CAFE_SEG)) {
                hw_CafeServido();
            	count_seg=0;
                ev5HZ_raised=0;
                hw_IndicadorApagado();
                estado_indicador=false;
                state = ALARMA;
            }
            
            if (ev5HZ_raised){
                toggleIndicador();
            	ev5HZ_raised=0;
            	state = SIRVIENDO_CAFE;
            }
            break;

        case ALARMA:
            if (evTick1seg_raised && (count_seg < ESPERA_ALARMA_SEG)) {
            	hw_SonarAlarma();
                count_seg++;
            }
            else if (evTick1seg_raised && (count_seg == ESPERA_ALARMA_SEG)) {
                hw_ApagarAlarma();
                count_seg=0;
                state = ESPERANDO_FICHA;
            }
            break;
    }

    clearEvents();
}

void fsm_maquina_raise_evSensorFicha(void)
{
    evSensorFicha_raised = 1;
}

void fsm_maquina_raise_evSensorCafe(void)
{
    evSensorCafe_raised = 1;
}

void fsm_maquina_raise_evSensorTe(void)
{
    evSensorTe_raised = 1;
}

void fsm_maquina_raise_evTick1seg(void)
{
    evTick1seg_raised = 1;
}

void fsm_maquina_raise_ev1HZ(void)
{
    ev1HZ_raised = 1;
}

void fsm_maquina_raise_ev5HZ(void)
{
    ev5HZ_raised = 1;
}

void fsm_maquina_printCurrentState(void)
{
    printf("Estado actual: %0d \n", state);
    printf("Segundos: %0d \n", count_seg);
}

/*==================[end of file]============================================*/
