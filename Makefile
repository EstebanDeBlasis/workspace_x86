TARGET_EXEC ?= ejecutable.bin

SRC_PATH := src
INC_PATH := inc
OUT_PATH := out
OBJ_PATH := $(OUT_PATH)/obj

vpath %.c $(SRC_PATH)
vpath %.o $(OBJ_PATH)

SOURCES := $(wildcard $(SRC_PATH)/*.c)
OBJS := $(subst $(SRC_PATH),$(OBJ_PATH),$(SOURCES:.c=.o))
OBJ_FILES := $(notdir $(OBJS))

CC = gcc
CFLAGS = -Wall
CPPFLAGS = -I$(INC_PATH)

%.o: %.c
	@echo "*** compiling C file $< ***"
	@mkdir -p $(OBJ_PATH)
	$(CC) $(CPPFLAGS) $(CFLAGS) -c $< -o $(OBJ_PATH)/$@
	@echo ""

$(TARGET_EXEC): $(OBJ_FILES)
	@echo "*** linking $@ ***"
	$(CC) $(OBJS) -o $(OUT_PATH)/$(TARGET_EXEC)
	@echo ""

run:
	@$(OUT_PATH)/$(TARGET_EXEC)

clean:
	@rm -f $(OUT_PATH)/*.* $(OBJ_PATH)/*.*
